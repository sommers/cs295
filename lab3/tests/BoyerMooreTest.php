<?php

require_once '../classes/BoyerMoore.php';

class BoyerMooreTest extends PHPUnit_Framework_TestCase
{
    protected $bm;
    protected function setUp()
    {
        $this->bm = new BoyerMoore();      
    }    
    
    /**
     * @dataProvider provider
     */
    public function testNumberOfMatches($haystack, $needle, $matches)
    {
        $this->bm->search($needle, $haystack);
        $this->assertEquals($matches, count($this->bm->getResults()));
    }    

    /**
     * @dataProvider badProvider
     * @expectedException InvalidStringException
     */
    public function testExceptions($haystack, $needle)
    {
        $this->bm->search($needle, $haystack);
    } 

    public function badProvider()
    {
        return array(
            array('', 'hello'),
            array('hello', ''),
            array('mulalah', 9),
            array(null, null),
            );
    }

    public function provider()
    {
        return array(
                array('abcdeabcdabcaba', 'abc', 3),
                array(<<<PASSAGE
The curved shore of Cruden Bay, Aberdeenshire, is backed by a waste of sandhills in whose hollows seagrass and moss and wild violets, together with the pretty “grass of Parnassus” form a green carpet. The surface of the hills is held together by bent-grass and is eternally shifting as the wind takes the fine sand and drifts it to and fro. All behind is green, from the meadows that mark the southern edge of the bay to the swelling uplands that stretch away and away far in the distance,[5] till the blue mist of the mountains at Braemar sets a kind of barrier. In the centre of the bay the highest point of the land that runs downward to the sea looks like a miniature hill known as the Hawklaw; from this point onward to the extreme south, the land runs high with a gentle trend downwards.
PASSAGE
                , 'grass', 3)
            );
    }

}
