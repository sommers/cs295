<?php
//lab 3
function my_autoloader($class) 
{
	include 'classes/' . $class . '.user.php';
}

spl_autoload_register('my_autoloader');


class UserTest extends PHPUnit_Framework_TestCase {
	/**
   *@dataProvider Provider
   */
	function testAuthenticate($username, $password, $success) {		
		$user = new User();
		$user->username = 'skyler';
		$user->hash = $user->hash_password('whatever');
		$result = $user->authenticate ($username, $password);
		$this->assertEquals($result, $success);
	}

	function Provider()	{	
		return array(
			array('skyler', 'whatever', true),
			array('skyler', 'WhatEver', false),
			array('skyler', ' ', false),
			array('', ' ', false), //should throw "Username and/or Password cannot be empty."
			array('sk', ' ', false), //should throw "Username must be a string with three or more characters."
		);
	}
}
