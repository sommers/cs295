<?php
class InvalidStringException extends Exception
{
}

class BoyerMoore
{
    public $needle = null;
    public $haystack = '';
    public $jumpTable = array();
    protected $results = array();

    public function search($needle, $haystack)
    {
        if (!is_string($needle) || strlen($needle) < 1) 
            throw new InvalidStringException("Needle must be a string with one or more characters.");
        if (!is_string($haystack) || strlen($haystack) < 1) 
            throw new InvalidStringException("Haystack must be a string with one or more characters.");

        $this->results  = array();
        $this->needle   = $needle;
        $this->haystack = $haystack;

        $this->deriveJumpTable(); 

        // Index of right-most character to test
        $currentCharacter = strlen($this->needle) - 1;
        $substringLength  = strlen($this->needle);
        $bufferLength     = strlen($this->haystack);

        while ($currentCharacter < $bufferLength) {
            for ($i = $substringLength - 1; $i >= 0; $i--) {
                // match
                $indx = $currentCharacter - $substringLength + $i + 1;
                if ($this->haystack[$indx] == $this->needle[$i]) {
                    // full match
                    if ($i == 0) {
                        $this->results[] = $currentCharacter - $substringLength + 1;
                        $currentCharacter += strlen($this->needle);
                    } else {
                        continue;
                    } 
                // mismatch
                } else {
                    $currentCharacter += $this->getJumpLength($this->haystack[$currentCharacter]);
                    break;
                }
            }
        }
    }

    private function deriveJumpTable()
    {
        $maxJump = strlen($this->needle);
        for ($i = $maxJump - 2; $i >= 0; $i--) {
            if (!array_key_exists($this->needle[$i], $this->jumpTable))
                $this->jumpTable[$this->needle[$i]] = $maxJump - 1 - $i;
        }
    }

    public function getJumpLength($character)
    {
        if (array_key_exists($character, $this->jumpTable)) {
            return $this->jumpTable[$character];
        } else {
            return strlen($this->needle);
        }
    }

    public function getJumpTable()
    {
        return $this->jumpTable;
    }

    public function getResults()
    {
        return $this->results;
    }
}

