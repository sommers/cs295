<?php
//lab 3
class InvalidStringException extends Exception
{
}
define('BCRYPT_COST', 10);
define('PASSWORD_SALT', 'whatever');
	
class User {
	public $username;
	public $firstname;
	public $lastname;
	public $hash;
	
	public function authenticate($username, $password) {
		$User = $this->username;
		$guessHash = self::hash_password($password);
			
		if(empty($username) || empty($password))
			throw new Exception("Username and/or Password cannot be empty.");
			
		if (!is_string($username) || strlen($username) < 3) 
            throw new InvalidStringException("Username must be a string with three or more characters.");		
		
		return ($guessHash === $this->hash && 
			$username === $User);	
	}
	
	public static function hash_password($password)	{
		return crypt($password, '$2a$' . BCRYPT_COST . '$' . PASSWORD_SALT . '$');
	}
}
