#!/usr/bin/env python

import json, struct

f = open('header.cap','rb')
hdr = f.read()
f.close()

version = struct.unpack("!B",hdr[0])[0]
version >>=4
print("%x" %version) 

IHL = struct.unpack("!B",hdr[0])[0]
IHL &=0b00000101
IHL = (IHL * 32)/8
print(IHL)

DSCP = struct.unpack("!B",hdr[1])[0]
DSCP >>=2
print("%x" %DSCP) 

ECN = struct.unpack("!B",hdr[1])[0]
ECN &=0b00000001
print("%x" %ECN) 

Length = struct.unpack("!B",hdr[2])[0]
Length2 = struct.unpack("!B",hdr[3])[0]
Length <<=8
Length += Length2
print(Length)

Idenify = struct.unpack("!B",hdr[4])[0]
Idenify2 = struct.unpack("!B",hdr[5])[0]
Idenify <<=8
Idenify += Idenify2
print(Idenify)

Flag3 = struct.unpack("!B",hdr[6])[0]
Flag3 >>=7
print("%x" %Flag3)

Flag2 = struct.unpack("!B",hdr[6])[0]
Flag2 >>=6
Flag2 &=0b00000001
print("%x" %Flag2)

Flag1 = struct.unpack("!B",hdr[6])[0]
Flag1 >>=5
Flag1 &=0b00000001
print("%x" %Flag1)

FlagOffset = struct.unpack("!B",hdr[6])[0]
FlagOffset2 = struct.unpack("!B",hdr[7])[0]
FlagOffset <<=8
FlagOffset += FlagOffset2
FlagOffset &= 0b00011111111111
print(FlagOffset)

Time = struct.unpack("!B",hdr[8])[0]
print(Time)

Protocol = struct.unpack("!B",hdr[9])[0]
print(Protocol)

Check = struct.unpack("!B",hdr[10])[0]
Check2 = struct.unpack("!B",hdr[11])[0]
Check <<=8
Check += Length2
print("%x" %Check)

SourceIP = struct.unpack("!B",hdr[12])[0]
SourceIP2 = struct.unpack("!B",hdr[13])[0]
SourceIP3 = struct.unpack("!B",hdr[14])[0]
SourceIP4 = struct.unpack("!B",hdr[15])[0]
print(str(SourceIP) + "." + str(SourceIP2) + "." + str(SourceIP3) + "." + str(SourceIP4))

DestinationIP = struct.unpack("!B",hdr[16])[0]
DestinationIP2 = struct.unpack("!B",hdr[17])[0]
DestinationIP3 = struct.unpack("!B",hdr[18])[0]
DestinationIP4 = struct.unpack("!B",hdr[19])[0]
print(str(DestinationIP) + "." + str(DestinationIP2) + "." + str(DestinationIP3) + "." + str(DestinationIP4))

python_dict = {'Version' : version,
'Internet Header Length' : IHL,
'Differentiated Services Code Point': DSCP,
'Explicit Congestion Notification': ECN,
'Totoal Length': Length,
'Identification': Idenify,
'Flag One': Flag1,
'Flag Two': Flag2,
'Flag Three': Flag3,
'Flag Offset': FlagOffset,
'Time To Live': Time,
'Protocol': Protocol,
'Header Checksum': Check,
'Source IP Address': (str(SourceIP) + "." + str(SourceIP2) + "." + str(SourceIP3) + "." + str(SourceIP4)),
'Destination IP Address': (str(DestinationIP) + "." + str(DestinationIP2) + "." + str(DestinationIP3) + "." + str(DestinationIP4))}
print(json.dumps(python_dict))


