#!/usr/bin/env python
import socket, struct


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.connect(('127.0.0.1', 1234))
Quote = 'It is working'

msg_type =  1
version = 1

payload_length = len(Quote)
data = struct.pack('!BBH', version, msg_type, payload_length)
print(s.send(data + Quote))

data = s.recv(4)
version, msg_type, payload_length = struct.unpack('!BBH', data)
Quote = s.recv(payload_length)
print(version, msg_type, payload_length, Quote)

