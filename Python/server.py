#!/usr/bin/env python
import socket, struct, json, random

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

server.bind (('', 1234))
server.listen(3)

while 1:
	conn, addr = server.accept()
	data = conn.recv(4)
	version, msg_type, payload_length = struct.unpack('!BBH', data)
	Quote = conn.recv(payload_length)
	print(version, msg_type, payload_length, Quote)
	
	if msg_type == 1:
		response = Quote
	
	if msg_type == 2:
		response = json.dumps(random.random())
		
	msg_type =  0
	version = 1
	payload_length = len(response)
	hdr = struct.pack('!BBH', version, msg_type, payload_length)
	print(conn.send(hdr + response))
		