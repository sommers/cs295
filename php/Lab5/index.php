<?php
require_once("recipe.php");
require_once("view.php");
$view = NULL;

    if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['action']) && $_GET['action'] == "insert") {
	$view = new View("insert", NULL);
   
    } elseif($_SERVER['REQUEST_METHOD'] == "POST"){
	$view = new View("thanks", NULL);
    
    } else { 
	$recipe = new RecipeModel();
	$view = new View("list", $recipe->findAll());
    }

$view->Render();


 