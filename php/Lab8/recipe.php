<?php
require_once("model.php");
class Recipe{
    public $id;
	public $title;
    public $ingredient0;
    public $ingredient1;
    public $ingredient2;
    public $instructions;

    function __construct($id, $title, $ing0, $ing1, $ing2, $inst){
        $this->id = $id;
        $this->title = $title;
        $this->ingredient0 = $ing0;
        $this->ingredient1 = $ing1;
        $this->ingredient2 = $ing2;
        $this->instructions = $inst;
    }
}

class RecipeModel extends Model{
    
    function findAll(){
		include_once ('connection.php');
		$conn = get_connection();
		$query = "SELECT * from recipe";
		$res = $conn->query($query);
		while ($row = $res->fetch_assoc()) {
        $dummyData [] = new Recipe(
			$row{'id'}, 
			$row{'title'}, 
			$row{'ingredient0'}, 
			$row{'ingredient1'}, 
			$row{'ingredient2'}, 
			$row{'instructions'});
		}
	return $dummyData;
	$res->free();
    }
	
	function insertData(){
		$conn = get_connection();
		$query = $conn->prepare("INSERT INTO recipe (title, ingredient0, ingredient1, ingredient2, instructions) VALUES (?,?,?,?,?)");      
		$query->bind_param('sssss', $_POST["title"], $_POST["ingredient0"],	$_POST["ingredient1"],	$_POST["ingredient2"],	$_POST["instructions"]);
		$query->execute();
	}
	
	function deleteData($delete){
		$conn = get_connection();
		$query = $conn->prepare("DELETE FROM recipe WHERE id=?");      
		$query->bind_param('s', $delete);
		$query->execute();
		setcookie('user', 'user', time() - 60, '/');
	}
	
	function tryUser(){
		$cost = '5';
		$salt = 'whatever';
		$user = htmlentities($_POST['user']);
		$pass = htmlentities($_POST['password']);
		$hpass = crypt($pass, '$2a$' . $cost . '$' . $salt . '$');
		$conn = get_connection();
		$res = $conn->query("SELECT user,password FROM users WHERE user='".$user."'");
		$row = $res->fetch_assoc();
		$res->free();
		if($row{'password'}==$hpass){
			setcookie('user', $user, time() + 60, '/');
		}
	}	
}
	





