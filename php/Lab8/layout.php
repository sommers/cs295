<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab8 MySQL</title>
        <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/default.css" rel="stylesheet" media="screen">
    </head>
    <body>                
        <div class="container">
            <h1 class="title">Lab 8 MySQL: Recipe Site</h1>  
            <?php echo $content; ?>  
			 <h6><?php
				 if (isset($_COOKIE['user'])){
					echo "User: ".$_COOKIE['user']." is logged in.";
				} else {
					echo "No user logged in";
				}
			?></h6>					
        </div>        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>