<?php
require_once("recipe.php");
require_once("view.php");
require_once("model.php");
require_once ('connection.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);

$view = NULL;
if(isset($_COOKIE['user'])){
    if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['action']) && $_GET['action'] == "insert") {
	$view = new View("insert", NULL);
   
    } elseif($_SERVER['REQUEST_METHOD'] == "POST"){
	$view = new View("thanks", NULL);
	
	$insert = new RecipeModel();
	$insert->insertData();
	
	} elseif($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['delete'])) {
	$recipe = new RecipeModel();
	$deleteID = $_GET["delete"];
	$recipe->deleteData($deleteID);
	$view = new View("login", NULL);
		
    } else { 
	$recipe = new RecipeModel();
	$view = new View("list", $recipe->findAll());
    }
}
else {	
	if($_SERVER['REQUEST_METHOD'] == "POST"){
	$view = new View("loginthanks", NULL);
	
	$insert = new RecipeModel();
	$insert->tryUser();
	
	} else { 
	$view = new View("login", NULL);
	}
}
$view->Render();


 