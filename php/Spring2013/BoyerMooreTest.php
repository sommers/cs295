<?php
require_once '../classs/BoyerMoore.php';

class BoyerMooreTest extends PHPUnit_Framework_TestCase {

	/**
	* @dataProvider provider
	*/

	public function testNumberOfMatches($haystack, $needle, $matches) {
		
		$bm = new BoyerMoore();
		
		$bm->search("abc", "abcdeabcdabcababc");
		$this->assertEquals(4, count($bm->getResults()));
	}
	
	public function provider () {
	
		#haystack, needle, matches
		return array(
			array("abcdeabcdabcababc", "abc", 4),
			array("", "abc", 0),
			array("abc", "", 0),
			array("this is our house.", "our", 1)
		);
	}
}