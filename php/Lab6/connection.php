<?php
function get_connection(){
    if (isset($_ENV['OPENSHIFT_APP_NAME'])) {
        define("DB_NAME", "myapp");
        define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
        define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
        define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);
    }
    else {
        define("DB_NAME", "myapp");
        define("DB_HOST", "localhost");
        define("DB_USER", "root");
        define("DB_PASS", "");
    }


    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_errno()) {
        echo "connection failed";
        die;
    }

    return $conn;
}
?> 
	