<?php
 class View {
    function __construct($page, $data) {
	$this->data = $data;
	$this->page = $page;
    }
	
    function Render() {
        ob_start();
        include $this->page . '.php';
        $content = ob_get_clean();
	require_once 'layout.php';
    }
}

